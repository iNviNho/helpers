#!/bin/bash

YEAR=$(date +%Y)
MONTH=$(date +%m)
DAY=$(date +%d)
HOUR=$(date +%H)
MINUTE=$(date +%M)

docker exec -it database mysqldump -uuser -ppassword database > databasedump-$YEAR$MONTH$DAY$HOUR$MINUTE.sql